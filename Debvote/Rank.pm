package Debvote::Rank;  # assumes Some/Module.pm

use strict;

BEGIN {
	use Exporter   ();
	use vars       qw($VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);

	# set the version for version checking
	$VERSION     = 1.00;

	@ISA         = qw(Exporter);
	@EXPORT      = qw(&ParseRank &InitRank &MakeBallot);
	%EXPORT_TAGS = ( );     # eg: TAG => [ qw!name1 name2! ],

	# your exported package globals go here,
	# as well as any optionally exported functions
	@EXPORT_OK   = qw(&InitRank &ParseRank &MakeBallot);
}

use vars      @EXPORT_OK;
use Debvote::Config qw(%Globals);
use Debvote::Email qw(&ProcessTags &InitEmailTags &LoadEmail %gtags);

# all file-scoped lexicals must be created before
# the functions below that use them.
# file-private lexicals go here
#############################################################################
#  Gloabal Variable Declaration
#############################################################################
my $FILE = '__FILE__';					#File name
my $bad_rank_letter_file = "/etc/debvote/letters/badrank.txt";
my $dup_rank_letter_file = "/etc/debvote/letters/duprank.txt";
my $skip_rank_letter_file = "/etc/debvote/letters/skiprank.txt";
#my $no_ballot_letter_file = "/etc/debvote/letters/noballot.txt";
my $choices=0;
my @bad_rank_letter;
my @dup_rank_letter;
my @skip_rank_letter;
#my @no_ballot_letter;
my %choice;
my %tags= (	"SECRETARY_TITLE" => "Debian Project Secretary",
			"SECRETARY_NAME" => "Darren Benham",
			"ERRORS_TITLE" => "Nobody",
			"ERRORS_EMAIL" => "errors\@benham.net",
			"VOTE_TITLE" => "Set Vote Title",
			"SECRETARY_EMAIL" => "secretary\@debian.org");

#############################################################################
#  Read Config File and parse
#############################################################################
sub InitRank
{	my @config = @_;
	
	print "Initializing Rank Method\n" if $Globals{ 'verbose' };
	for( my $i=0; $i<=$#config; $i++)
	{	$_ = $config[$i];
		chop $_;
		next unless length $_;
		next if /^#/;
		if ( /^Choices\s*[:=]\s*([^#]*)\s*(#.*)?/i )
		{	$choices = int $1;
			print "D2: (config) Choices=$choices\n" 
				if $Globals{ 'debug' } > 1;
		}
		elsif ( /^Choice\s+#*(\d+)\s*[:=]\s*([^#]*)/i )
		{	$choice{ $1 } = $2;
			print "D2: (config) Choice $1=$choice{$1}\n" 
				if $Globals{ 'debug' } > 1;
		}
		elsif( /^Dup Rank Letter\s*[:=]\s*([^#]*)/i )
		{	$dup_rank_letter_file = $1;
			print "D2: (config) Dup Rank Letter=$bad_rank_letter_file\n" 
				if $Globals{ 'debug' } > 1;
		}
		elsif( /^Skip Rank Letter\s*[:=]\s*([^#]*)/i )
		{	$skip_rank_letter_file = $1;
			print "D2: (config) Skip Rank Letter=$skip_rank_letter_file\n" 
				if $Globals{ 'debug' } > 1;
		}
		elsif( /^Bad Rank Letter\s*[:=]\s*([^#]*)/i )
		{	$bad_rank_letter_file = $1;
			print "D2: (config) Bad Rank Letter=$dup_rank_letter_file\n" 
				if $Globals{ 'debug' } > 1;
		}
	}
	&::fail( "E: `Choices' must be > 1" ) unless $choices > 1;
	for( my $i=1; $i<=$choices; $i++ )
	{ &::fail( "E: `Choice $i' is missing" ) unless defined $choice{$i}; }

	#init tags from global variables
	$gtags{ 'VOTE_TITLE' } = $Globals{ 'title' };

	#create response messages
	@skip_rank_letter = &LoadEmail( $skip_rank_letter_file );
	print "D3: Skip Rank Letter = @skip_rank_letter\n" if $Globals{ 'debug' } > 2;
	
	#create response messages
	@bad_rank_letter = &LoadEmail( $bad_rank_letter_file );
	print "D3: Bad Rank Letter = @bad_rank_letter\n" if $Globals{ 'debug' } > 2;
	
	#create response messages
	@dup_rank_letter = &LoadEmail( $dup_rank_letter_file );
	print "D3: Dup Rank Letter = @dup_rank_letter\n" if $Globals{ 'debug' } > 2;
}

sub ParseRank
{	my @ballot = @_;
	my %rank;
	my %used;
	my $valid = 0;
	my $return = '';

	for( my $i=0; $i<=$#ballot; $i++ )
	{	$_ = $ballot[$i];
		next unless /\[\s*(\d+)\s*\]\s*Choice\s+(\d+):/;
		$valid = 1;
		&::reply( @dup_rank_letter ) if defined $used{ $1 };
		&::reply( @bad_rank_letter ) if $1 > $choices;
		print "D2: (parse) Choice $1=$2\n" if $Globals{ 'debug' } > 1;

		$used{ $1 } = $2;
		$rank{ $2 } = $1;
	}

	return undef unless $valid;
#	&::reply( @no_ballot_letter ) unless $valid;

	#check for not skipping in the ranking
	for( my $i=1; $i<=$choices; $i++ )
	{	&::reply( @skip_rank_letter ) if ( $rank{ $i } > (keys %rank) );
		$return .= $rank{ $i } if defined $rank{ $i };
		$return .= '-' if not defined $rank{ $i };
	}
	return $return;
}

sub MakeBallot
{
	my $str;
	my $body='';

	for( my $i=1; $i<=$choices; $i++ )
	{	$str = "[   ] Choice $i: $choice{ $i }\n";
		$body .= $str;
	}
	return $body;
}

sub TallyRank
{   my @ballot = @_;
    my %rank;
    my %used;
    my $valid = 0;
    my $return = '';

    for( my $i=0; $i<=$#ballot; $i++ )
    {   $_ = $ballot[$i];
        next unless /\[\s*(\d+)\s*\]\s*Choice\s+(\d+):/;
        $valid = 1;
        &::reply( @dup_rank_letter ) if defined $used{ $1 };
        &::reply( @bad_rank_letter ) if $1 > $choices;
        print "D2: (parse) Choice $1=$2\n" if $Globals{ 'debug' } > 1;

	$used{ $1 } = $2;
        $rank{ $2 } = $1;
    }

    return undef unless $valid;

    #check for not skipping in the ranking
    for( my $i=1; $i<=$choices; $i++ )
    {	&::reply( @skip_rank_letter ) if ( $rank{ $i } > (keys %rank) );
    	$return .= $rank{ $i } if defined $rank{ $i };
    	$return .= '-' if not defined $rank{ $i };
    }
    return $return;
}


sub FindWinner
{
    my %DataBase = @_;
    my %pref = ();

# initialize to zero
    for( my $i=0; $i<$choices-1; $i++ )
    {   my $choice1 = $i+1;
        for ( my $j=$i+1; $j<$choices; $j++ )
        {   my $choice2 = $j+1;
	    $pref{ "$choice1-$choice2" } = 0;
	    $pref{ "$choice2-$choice1" } = 0;
	}
    }

    foreach my $key ( keys( %DataBase ) )
    {
        my $rec = {};
        my @ballot = undef;

	$rec = $DataBase{ $key };
        @ballot = split( "",  $rec->{ 'vote' } );
#	 @test = split( "", $rank );
        for( my $i=0; $i<$choices-1; $i++ )
        {   my $choice1 = $i+1;
	    for ( my $j=$i+1; $j<$choices; $j++ )
            {   my $choice2 = $j+1;
		if ( $ballot[$i] ne '-'  and $ballot[$j] ne '-' and $i != $j ) 
	        {   if ( $ballot[$i] < $ballot[$j] )
		    {   $pref{ "$choice1-$choice2" }++; }
		    else
		    {   $pref{ "$choice2-$choice1" }++; }
		} 
	    }
	}
    }


    for( my $i=0; $i<$choices; $i++ )
    {   my $choice1 = $i+1;

	print "Choice #$choice1: " . $choice{ "$choice1" } . "\n";
	for ( my $j=0; $j<$choices; $j++ )
	{   my $choice2 = $j+1;
	    next if $i == $j;
	    my $temp1 = "$choice1-$choice2";
	    my $temp2 = "$choice2-$choice1";

#        if ( defined $preference{ "$choice1-$choice2" } )
#        { print $preference{ "$temp1" } . " ballots rank $choice1 over than $choice2\n"; }
#        else { print "nobody prefers $choice1 over $choice2\n"; }
#        if ( defined $preference{ "$choice2-$choice1" } )
#        { print $preference{ "$choice2-$choice1" } . " ballots rank $choice2 over than $choice1\n"; }
#        else { print "nobody prefers $choice2 over $choice1\n"; }

	    if ( defined $pref{ "$temp1" } )
	    {   if ( defined $pref{ "$temp2" } )
		{   if ( $pref{ "$temp1" } > $pref{ "$temp2" } )
		    { print "\tis prefered to Choice #$choice2: " . $choice{ "$choice2" } . " (".$pref{ "$temp1" }."-".$pref{ "$temp2" }.")\n"; }
		    elsif ( $pref{ "$temp2" } == $pref{ "$temp1" } )
		    { print "\tties with $choice2 (".$pref{ "$temp1" }."-".$pref{ "$temp2" }.")\n"; }
		}
		else
		{ print "\tis prefered to $choice2 (".$pref{ "$temp1" }."-".$pref{ "$temp2" }.")\n"; }
	    }
	    else 
	    {   if ( not defined $pref{ "$temp2" } )
		{ print "\tties with $choice2 (".$pref{ "$temp1" }."-".$pref{ "$temp2" }.")\n"; }
	    }
	}
    }
}

END { }       # module clean-up code here (global destructor)
1;
