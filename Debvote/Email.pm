package Debvote::Email;  # assumes Some/Module.pm

use strict;

BEGIN {
	use Exporter   ();
	use vars       qw($VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);

	# set the version for version checking
	$VERSION     = 1.00;

	@ISA         = qw(Exporter);
	@EXPORT      = qw(&LoadEmail &ProcessTags &InitEmailTags);
	%EXPORT_TAGS = ( );     # eg: TAG => [ qw!name1 name2! ],

	# your exported package globals go here,
	# as well as any optionally exported functions
	@EXPORT_OK   = qw(&LoadEmail &InitEmailTags &ProcessTags %gtags);
}

use vars      @EXPORT_OK;
use Debvote::Config qw(%Globals);

# initialize package globals, first exported ones
%gtags= (	"SECRETARY_TITLE" => "Debian Project Secretary",
		"SECRETARY_NAME" => "Darren Benham",
		"ERRORS_TITLE" => "Nobody",
		"ERRORS_EMAIL" => "errors\@benham.net",
		"VOTE_TITLE" => "Set Vote Title",
		"SECRETARY_EMAIL" => "secretary\@debian.org");

#############################################################################
#  Initialize Global Tags
#############################################################################
sub InitEmailTags
{   my @config = @_;
	
    print "V: Initializing Email Tags\n" if $Globals{ 'verbose' };
    for( my $i=0; $i<=$#config; $i++)
    {	$_ = $config[$i];
	chop $_;
	next unless length $_;
	next if /^#/;
	if ( /^GTAG\s*[:=]\s*(\S+)\s*[:=]\s*([^#]*)/i )
	{   $gtags{ $1 } = $2;
	    print "D2: (email) GTag $1=$gtags{$1}\n" if $Globals{ 'debug' } > 1;
	}
    }
}

#############################################################################
#  Load File with Tags
#############################################################################
sub LoadEmail
{   my $emailfile = $_[0];
    my @email;

    open( LETTER, $emailfile ) or &::fail( "Unable to open $emailfile: $!" );
    @email = <LETTER>;
    close LETTER;
    &ProcessTags( \@email, \%gtags, "GTAG" );
    return @email;
}
#############################################################################
#  Process Tags
#############################################################################
sub ProcessTags
{   my ($email, $tagsin, $marker) = @_;
    my %tags=%$tagsin;
    my $tag;

    print "V: Processing Template Mail\n" if $Globals{ 'verbose' };
    foreach my $line ( @$email )
    {	while( $line =~ /\%$marker\_(\S*)\%/s )
	{   if( defined( $tags{ $1 } ) ) { $tag = $tags{ $1 }; }
	    else { $tag = "(missed tag $1)"; }
	    $line =~ s/\%$marker\_(\S*)\%/$tag/;
	}
    }
    1;
}

END { }       # module clean-up code here (global destructor)
1;
