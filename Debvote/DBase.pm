package Debvote::DBase;  # assumes Some/Module.pm

use strict;

BEGIN {
	use Exporter   ();
	use vars       qw($VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);

	# set the version for version checking
	$VERSION     = 1.00;

	@ISA         = qw(Exporter);
	@EXPORT      = qw(&ReadDB &WriteDB);
	%EXPORT_TAGS = ( );     # eg: TAG => [ qw!name1 name2! ],

	# your exported package globals go here,
	# as well as any optionally exported functions
	@EXPORT_OK   = qw(&ReadDB &WriteDB);
}

use vars      @EXPORT_OK;
use Debvote::Config qw(%Globals);

# non-exported package globals go here
#use vars      qw(@more $stuff);

# initialize package globals, first exported ones

# then the others (which are still accessible as
# $Some::Module::stuff)

# all file-scoped lexicals must be created before
# the functions below that use them.

# file-private lexicals go here

sub ReadDB
{	my $file = $_[0];
	my $name; my $addr; my $id; my $vote;
	my %dbHash;

	print "V: Reading Database: $file\n" if $Globals{ 'verbose' };
	open FILE, "$file" || die "Opening $file: $!";
	while( <FILE> )
	{
		my $rec = {};
		next unless /^I: (.*)$/;
		$id = $1;
		next unless <FILE> =~ /^N: (.*)$/;
		$name = $1;
		next unless <FILE> =~ /^A: (.*)$/;
		$addr = $1;
		next unless <FILE> =~ /^V: (.*)$/;
		$vote = $1;
		$dbHash{ "$id" } = $rec;
		$rec->{ "name" } = $name;
		$rec->{ "addr" } = $addr;
		$rec->{ "vote" } = $vote;
	}
	close FILE;
	return %dbHash;
}

sub WriteDB
{	my ($file, %DataBase) = @_;
	my %dbHash;

	print "V: Writing Database: $file\n" if $Globals{ 'verbose' };
	open FILE, ">$file" || die "Opening $file: $!";

	foreach my $i ( keys %DataBase )
	{
#		print "key = $i\n";
		print FILE "I: $i\n";
		my $rec = $DataBase{ "$i" };
		print FILE "N: $rec->{ name }\n";
		print FILE "A: $rec->{ addr }\n";
		print FILE "V: $rec->{ vote }\n";
	}
	close FILE;
}

END { }       # module clean-up code here (global destructor)
1;
