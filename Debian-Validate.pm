package Debvote::Validate;  # assumes Some/Module.pm

use strict;

BEGIN {
	use Exporter   ();
	use vars       qw($VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);

	# set the version for version checking
	$VERSION     = 1.00;

	@ISA         = qw(Exporter);
	@EXPORT      = qw(&ValidateBallot);
	%EXPORT_TAGS = ( );     # eg: TAG => [ qw!name1 name2! ],

	# your exported package globals go here,
	# as well as any optionally exported functions
	@EXPORT_OK   = qw(&ValidateBallot @replyto %voter @ballotbody)
}

use vars      @EXPORT_OK;
use Mail::Address;

# initialize package globals, first exported ones
%voter = ();
@ballotbody = "";
@replyto = undef;

#############################################################################
#  Parse Email Header
#############################################################################
#my $i;
#for ($i=0; $i<=$#ballot; $i++) 
#{ 	$_ = $ballot[$i];
#	chop $_;
#    last unless length( $_ );
#	print "D3: (header parse) $_\n" if $Globals{ 'debug' } > 2;
#    if (s/^(\S+):\s*//) 
#	{ 	my $v= $1; $v =~ y/A-Z/a-z/;
#        print "D2: (header parse) $v=$_\n" if $Globals{ 'debug' } > 1;
#        $header{ $v } = $_;
#    } else { print "D2: (header parse fail) $_\n" if $Globals{ 'debug' } > 1; }
#}
#defined($header{'from'}) || &fail("no From header");
#$replyto = $ENV{ "REPLYTO" };
#$body .= join( "", @ballot[$i..$#ballot] );

#############################################################################
#  Authenticate Email
#############################################################################

sub ValidateBallot
{   my @ballot = @_;

    @replyto = Mail::Address->parse( $ENV{ "PGP_KEYNAME" });
    @ballotbody = @ballot;
    $voter{ 'name' } = ($replyto[0])->name();
    $voter{ 'email' } = ($replyto[0])->address();
    $voter{ 'id' } = $ENV{ 'PGP_KEYID' };
}

END { }       # module clean-up code here (global destructor)
