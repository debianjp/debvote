To: %LTAG_TO%
From: %GTAG_SECRETARY_TITLE% <%GTAG_SECRETARY_EMAIL%>
Errors-To: %GTAG_SECRETARY_TITLE% <%GTAG_SECRETARY_EMAIL%>
Reply-To: %GTAG_SECRETARY_TITLE% <%GTAG_SECRETARY_EMAIL%>
Subject: No Ballot Found

There was no ballot found in your email.  The body of which is included
below.
Please re-send your vote from the Call For Votes (CFV).

The body of your message follows.

- %GTAG_SECRETARY_NAME%
  (%GTAG_SECRETARY_TITLE%)

--------------------------------------------------------------------------
%LTAG_BODY%
