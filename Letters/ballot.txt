To: debian-devel-announce@lists.debian.org
Cc: debian-vote@lists.debian.org
From: %GTAG_SECRETARY_TITLE% <%GTAG_SECRETARY_EMAIL%>
Errors-To: %GTAG_SECRETARY_TITLE% <%GTAG_SECRETARY_EMAIL%>
Reply-To: leader00@vote.debian.org
Subject: [BALLOT] %GTAG_VOTE_TITLE%

                        CALL FOR VOTES
         
    Votes must be received by 23:59:59 UTC Mar 9, 2000

This vote is being conducted as required by the Debian Constitution.  For
voting questions only contact secretary@debian.org.  

HOW TO VOTE

Do not erase anything between these
lines and do not change the group names.


In the brackets next to your preferred choice, place a 1.
Place a 2 in the brackets next to your next choice.  Continue till
you rank your last choice.  You may leave choices you consider
unacceptable blank.  Start with 1, don't skip any numbers, don't repeat.
To vote "no, no matter what" do not leave an option black but rank "Further
Discussion" higher than the unacceptable choices.

Then mail the ballot to: leader00@vote.debian.org.  Just Replying to this
message should work, but check the "To:" line.  Don't worry about spacing
of the columns or any quote characters (">") that your reply inserts.
NOTE:  The vote must be PGP signed with your key that is in the debian keyring

-=-=-=-=-=- Don't Delete Anything Between These Lines =-=-=-=-=-=-=-=-
%GTAG_BODY%
-=-=-=-=-=- Don't Delete Anything Between These Lines =-=-=-=-=-=-=-=-

- %GTAG_SECRETARY_NAME%
  (%GTAG_SECRETARY_TITLE%)
